<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fetch</title>
</head>
<body>
    <form action="#" method="post">
        <input type="text" name="nome">
        <input type="number" name="idade" id="number">
        <button type="submit">Enviar</button>
    </form>
    <p class="nome"></p>
    <p class="email"></p>
    <script src="main.js"></script>
</body>
</html>
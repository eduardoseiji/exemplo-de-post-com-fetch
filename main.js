const form      = document.querySelector('form');
const nome      = document.querySelector('.nome');
const email     = document.querySelector('.email');

form.addEventListener('submit', handleSubmit);

async function handleSubmit(e) {
    e.preventDefault();

    const formData  = new FormData(this);
    const url       = './form.php';
    const options   = {
        method: 'POST',
        headers: new Headers(),
        body: formData
    }
    
    await fetch(url, options) 
    .then(r => r.json())
    .then(r2 => {
        nome.innerText  = r2.nome;        
        email.innerText = r2.idade;
    })
    .catch((err) => console.log(err)) 
}